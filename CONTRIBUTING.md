欢迎各位完善本仓库。我们也欢迎提交表番字幕，不过为了查看方便需要放在二级目录中。

# 贡献者

![LiuliPack](https://www.gravatar.com/avatar/cff01b340dcbff0c89d93310f95f3195?s=60)<br>LiuliPack | 贡献者占位 | 贡献者占位 | 贡献者占位 | 贡献者占位
-- | - | - | - | -
贡献者占位 | 贡献者占位 | 贡献者占位 | 贡献者占位 | 贡献者占位


# 规范 / Standard

**文件命名**
```
[空轴作者_片源来源-规格][作品发行]作品名.ass

[LiuliPack_hikiko123-480P][魔人 petit]錬精術士コレットのHな搾精物語 第一話 コレットのアトリエ開業！ 爆乳勇者シルヴィアとの出会い.ass
```


**打轴时**
- Aegisub 建议调整软件参数： 
  - `音频框-水平缩放(从左往右数第一个滑块)` 建议调整为 100%；
  - `音频框-同时调节垂直缩放与音量(从左往右数第二、三个滑块下方按钮)` 调整为关闭；
  - `音频框-垂直缩放(从左往右数第二个滑块)` 建议调整为 100%；
  - `音频框-音量(从左往右数第三个滑块)` 建议调整为 8%-55%。为了保证听力不受损，建议在能听清的前提下尽量调小音量。
  - 更多内容请移步至 [LiuliPack/AwesomeTiming#1-建议修改的设置项](https://gitlab.com/LiuliPack/knowledge/-/tree/main/AwesomeTiming#1-建议修改的设置项)。
- 为保证性器官不受潜在损伤，建议使用番茄工作法，并将工作时间调整为 20 分钟，休息时间调整为 10 分钟。软件推荐：[Reliak/Reliak-Timer](https://sourceforge.net/projects/reliak-timer)([GitHub](https://github.com/reliak/reliak-timer))
  - Reliak-Timer 建议调整参数：
    - `Worktime` 建议调整为 20；
    - `Breaktime` 建议调整为 10；
    - `More Settings…-General-Restart timer automaticall` 建议调整为启用；
    - `More Settings…-General-Show starter info panel` 建议调整为禁用；
    - `More Settings…-Alarm notifications-Flash window` 建议调整为禁用；
    - `More Settings…-Alarm notifications-Popup window` 建议调整为启用；
    - `More Settings…-Color-Work background-Advanced` 建议调整为 #FF607D8B；
    - `More Settings…-Color-Break background-Advanced` 建议调整为 #FF009688；
    - `More Settings…-Color-Work foreground-Advanced` 建议调整为 #8FFFFFFF；
    - `More Settings…-Color-Break foreground-Advanced` 建议调整为 #8FFFFFFF；
    - `More Settings…-Font-Font famliy` 建议调整为 [Anton](https://github.com/googlefonts/AntonFont/blob/main/fonts/Anton-Regular.ttf)；
    - `More Settings…-Font-Font size` 建议调整为 98；
    - `菜单(主窗口区域内右键)-Compact mode` 建议调整为启用。
- 低于 .7 秒间隔的对话，将前一句对话结束时间顺延到后一句对话开始时。
- 少于 .5 秒的对话，延长至 .5 秒。
- 句尾仅保留句尾拖长音。无意义的喘息声可忽略。
- 单独的语气词因与国语差异较小，因此默认将其注释，并在说话人栏中填入`语气词 / Auxiliary word`。常见的语气助词有，啊、嗯、诶、哼等。


# 讨论 / Discuss
[matrix](https://matrix.to/#/!WKcdKKiAhyNuWOUGqF:matrix.org)