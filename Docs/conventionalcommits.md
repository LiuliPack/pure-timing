这是约定式提交(https://www.conventionalcommits.org/zh-hans )的定制化版本，我把它称之为——约定式提交 1.1.0。


```
<修改类型>[涵盖范围]: <描述>
```

修改类型 | 全称 | 描述
-- | - | -
`feat` | 新增(Features) | 新增字幕。
`fix` | 错误修正(Bug Fixes) | 修正字幕错误。
`docs` | 文档(Documentation) | 仅修改文档。模板部分也算在此类型中。
`style` | 样式(Styles) | 不变更时轴信息，仅修改字幕样式。
`chore` | 后勤(Chores) | 不影响字幕文件的调整。
`revert` | 还原(Reverts) | 撤销上次的提交。


**参考**
- https://www.conventionalcommits.org/zh-hans
- https://github.com/pvdlg/conventional-changelog-metahub#commit-types